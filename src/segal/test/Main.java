package segal.test;

import segal.calc.Calculator;
import segal.calc.UnlinearIteration;

public class Main {

	public static void main(String[] args) {
		Calculator c = new UnlinearIteration();
		c.calc(0.1, 1e-4, 1000, 0.05, 650, Calculator.kappa, 1);
	}
}
