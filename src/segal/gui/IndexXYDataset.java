package segal.gui;

import org.jfree.data.xy.AbstractXYDataset;

public class IndexXYDataset extends AbstractXYDataset {
    private static final Comparable comp = new Comparable() {
        @Override
        public int compareTo(Object o) {
            return this.hashCode() - o.hashCode();
        }
    };

    private final double[][] data;
    private final double step;
    private int index = 0;

    public IndexXYDataset(double[][] data, double step) {
        this.data = data;
        this.step = step;
    }

    public void setIndex(int index) {
        this.index = index;
        fireDatasetChanged();
    }

    @Override
    public int getSeriesCount() {
        return 1;
    }

    @Override
    public Comparable getSeriesKey(int i) {
        return comp;
    }

    @Override
    public int getItemCount(int i) {
        check(i);
        return data[index].length;
    }

    @Override
    public Number getX(int i, int i2) {
        check(i);
        return i2 * step;
    }

    @Override
    public Number getY(int i, int i2) {
        check(i);
        return data[index][i2];
    }

    private void check(int i) {
        if (i != 0) {
            throw new IllegalArgumentException("index out of range");
        }
    }
}
