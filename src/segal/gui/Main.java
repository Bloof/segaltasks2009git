package segal.gui;

import segal.calc.Calculator;
import segal.calc.MatrixTriple;
import segal.calc.UnlinearIteration;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main extends JFrame {
	private static final int SPACE = 5;
    private final Calculator calc = new UnlinearIteration();

	private DoubleField heightField = new DoubleField("Высота", 0.05);
	private DoubleField startTField = new DoubleField("Температура", 650.0);
    private DoubleField dField = new DoubleField("D", Calculator.kappa);
    private DoubleField alphaField = new DoubleField("\u03b1", 1.0);

	private DoubleField dtField = new DoubleField("\u0394t", 0.1);
	private DoubleField dzField = new DoubleField("\u0394x", 1e-4);
	private IntegerField stepCountField = new IntegerField("Количество шагов", 1000);

	private Main() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("Параметры");
		setResizable(false);

		VBoxPanel mainPanel = new VBoxPanel();
		mainPanel.setBorder(BorderFactory.createEmptyBorder(SPACE, SPACE,
				SPACE, SPACE));

		mainPanel.addComponent(heightField);
		mainPanel.addComponent(startTField);
        mainPanel.addComponent(dField);
        mainPanel.addComponent(alphaField);

		mainPanel.addComponent(dtField);
		mainPanel.addComponent(dzField);
		mainPanel.addComponent(stepCountField);

		JButton calcButton = new JButton("Вычислить");
		calcButton.setMaximumSize(new Dimension(Integer.MAX_VALUE, calcButton
				.getPreferredSize().height));
		calcButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calculate();
			}
		});
		mainPanel.addComponent(calcButton);

		setContentPane(mainPanel);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private void calculate() {
		double height, startT, dt, dz, alpha, d;
		int stepCount;
		try {
			height = heightField.getValue();
			startT = startTField.getValue();
            d = dField.getValue();
            alpha = alphaField.getValue();
			dt = dtField.getValue();
			dz = dzField.getValue();
			stepCount = stepCountField.getValue();
		} catch (IllegalArgumentException e) {
			errorMessage("Неверный формат входных данных");
			return;
		}
        new GraphFrame(calc.calc(dt, dz, stepCount,
                        height, startT, d, alpha), dz);
//        final Calculator calc = new UnlinearIteration();
//        final ProgressMonitor monitor = new ProgressMonitor(this, "Message", null, 0, 100);
////        calc.addChangeListener(new ChangeListener() {
////            @Override
////            public void stateChanged(ChangeEvent e) {
////                System.out.println(calc.getStep());
////                monitor.setProgress(calc.getStep());
////            }
////        });
//        SwingWorker<MatrixTriple, Void> worker = new SwingWorker<MatrixTriple, Void>() {
//            @Override
//            protected MatrixTriple doInBackground() throws Exception {
//                final Calculator calc = new UnlinearIteration();
////                setP
//                calc.addChangeListener(new ChangeListener() {
//                    @Override
//                    public void stateChanged(ChangeEvent e) {
//                        setProgress(calc.getStep() * 100 / (stepCount - 1));
//                    }
//                });
//                return calc.calc(dt, dz, stepCount,
//                        height, startT, d, alpha);
//            }
//        };
//        worker.addPropertyChangeListener(new PropertyChangeListener() {
//            @Override
//            public void propertyChange(PropertyChangeEvent evt) {
//                if (evt.getPropertyName().equals("progress")) {
//                    monitor.setProgress((Integer)evt.getNewValue());
//                }
//            }
//        });
//        worker.execute();
////        Future<MatrixTriple> ans = Executors.newSingleThreadExecutor().submit(new Callable<MatrixTriple>() {
////            @Override
////            public MatrixTriple call() throws Exception {
////                return calc.calc(dt, dz, stepCount,
////                        height, startT, d, alpha);
////            }
////        });
////        MatrixTriple ans;
////        new Thread(new Runnable() {
////            @Override
////            public void run() {
////                 ans = calc.calc(dt, dz, stepCount,
////                        height, startT, d, alpha);
////                //To change body of implemented methods use File | Settings | File Templates.
////            }
////        }).start();
//
//        try {
//            new GraphFrame(worker.get(), dz);
//        } catch (InterruptedException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        } catch (ExecutionException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
    }

	private void errorMessage(String msg) {
		JOptionPane.showMessageDialog(this, msg, "Ошибка",
				JOptionPane.ERROR_MESSAGE);
	}

	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				new Main();
			}
		}).start();
	}
}
