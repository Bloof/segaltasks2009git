package segal.gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import segal.calc.MatrixTriple;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

public class GraphFrame extends JFrame {
    private final IndexXYDataset temperature;
    private final IndexXYDataset concentration;
    private final IndexXYDataset velocity;
    private final JSlider slider;

    public GraphFrame(MatrixTriple triple, double dz) {
        temperature = new IndexXYDataset(triple.getT(), dz);
        concentration = new IndexXYDataset(triple.getX(), dz);
        velocity = new IndexXYDataset(triple.getV(), dz);
        JFreeChart tChart = ChartFactory.createXYLineChart("температура", "z", "T", temperature, PlotOrientation.VERTICAL, false, false, false);
        JFreeChart cChart = ChartFactory.createXYLineChart("концентрация", "z", "C", concentration, PlotOrientation.VERTICAL, false, false, false);
        JFreeChart vChart = ChartFactory.createXYLineChart("скорость", "z", "V", velocity, PlotOrientation.VERTICAL, false, false, false);
        ChartPanel tPanel = new ChartPanel(tChart);
        ChartPanel cPanel = new ChartPanel(cChart);
        ChartPanel vPanel = new ChartPanel(vChart);
        HBoxPanel graphPanel = new HBoxPanel();
        graphPanel.addComponent(tPanel);
        graphPanel.addComponent(cPanel);
        graphPanel.addComponent(vPanel);
//        graphPanel.addComponent(tPanel);
//        graphPanel.addComponent(cPanel);
        slider = new JSlider(JSlider.HORIZONTAL, 0, triple.getT().length - 1, 0);
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
//                System.out.println(e.getSource());
                int index = slider.getValue();
                temperature.setIndex(index);
                concentration.setIndex(index);
                velocity.setIndex(index);
            }
        });
        int time = triple.getT().length;
        int pow = 1;
        while (pow * 10 < time) {
            pow *= 10;
        }
        time = (time / pow) * pow / 10;
        slider.setMajorTickSpacing(time);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(graphPanel, BorderLayout.CENTER);
        mainPanel.add(slider, BorderLayout.SOUTH);
//        graphPanel.addComponent(slider);
        add(mainPanel);
        pack();
        setVisible(true);
    }
}
