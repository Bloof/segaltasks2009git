package segal.calc;

public class Thomas {

	public static double[] solve(final double[][] A, final double[] b) {
		int len = b.length;
		double[] x = new double[len], b_ = new double[len], A_[] = new double[len][len];

		for (int i = 0; i < len; ++i) {
			for (int j = 0; j < len; ++j) {
				A_[i][j] = A[i][j];
			}
			b_[i] = b[i];
		}

		A_[0][1] /= A_[0][0];
		b_[0] /= A_[0][0];
		for (int i = 1; i < len - 1; ++i) {
			A_[i][i + 1] /= A_[i][i] - A_[i - 1][i] * A_[i][i - 1];
			b_[i] = (b_[i] - b_[i - 1] * A_[i][i - 1])
					/ (A_[i][i] - A_[i - 1][i] * A_[i][i - 1]);
		}
		b_[len - 1] = (b_[len - 1] - b_[len - 2] * A_[len - 1][len - 2])
				/ (A_[len - 1][len - 1] - A_[len - 2][len - 1]
						* A_[len - 1][len - 2]);

		x[len - 1] = b_[len - 1];
		for (int i = len - 2; i >= 0; --i) {
			x[i] = b_[i] - A_[i][i + 1] * x[i + 1];
		}

		return x;
	}
}
