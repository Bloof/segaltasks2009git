package segal.calc;

import javax.swing.event.ChangeListener;

public interface Calculator {

	public static final double THome = 300, ro = 830, lambda = 0.13, c = 1980,
			kappa = lambda / (ro * c), q = 7E5, q_c = q / c;

	public MatrixTriple calc(double dt, double dz, int steps, double length,
			double T0, double D, double alpha);

    public void addChangeListener(ChangeListener listener);

    public int getStep();
}
