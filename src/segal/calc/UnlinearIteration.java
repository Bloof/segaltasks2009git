package segal.calc;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.HashSet;
import java.util.Set;

public class UnlinearIteration implements Calculator {

	private static final int ITERATIONS = 1;
    private int step = 0;
	private EnergyFunction W;
    private final Set<ChangeListener> listeners = new HashSet<ChangeListener>();

	public UnlinearIteration() {
		W = new EnergyFunction();
	}

	@Override
	public MatrixTriple calc(double dt, double dz, int steps, double length,
			double T0, double D, double alpha) {
		
		// init default values
		W.setAlpha(alpha);
		int len = (int) Math.ceil(length / dz);
		double dz_2 = dz * dz;
		double[][] X = new double[steps][len], T = new double[steps][len];
		X[0][0] = 0;
		T[0][0] = T0;
		for (int i = 1; i < len; ++i) {
			X[0][i] = 1;
			T[0][i] = THome;
		}

		double[] AX[] = new double[len - 1][len - 1], bX = new double[len - 1];
		double[] AT[] = new double[len - 1][len - 1], bT = new double[len - 1];

		for (step = 1; step < steps; ++step) {
			X[step][0] = 0;
			T[step][0] = T0;

			// Ax=b, A - tridiagonal
			// calc X, T
			AX[0][0] = AX[len - 2][len - 2] = dz_2 + 2 * D * dt;
			AX[0][1] = AX[len - 2][len - 3] = -D * dt;
			bX[0] = (W.calc(X[step - 1][1], T[step - 1][1]) * dt + X[step - 1][1])
					* dz_2;
			bX[len - 2] = (W.calc(X[step - 1][len - 1], T[step - 1][len - 1])
					* dt + X[step - 1][len - 1])
					* dz_2 + D * dt * X[step - 1][len - 1];

			AT[0][0] = AT[len - 2][len - 2] = dz_2 + 2 * kappa * dt;
			AT[0][1] = AT[len - 2][len - 3] = -kappa * dt;
			bT[0] = dz_2
					* (-q_c * dt * W.calc(X[step - 1][1], T[step - 1][1]) + T[step - 1][1])
					+ kappa * dt * T0;
			bT[len - 2] = dz_2
					* (-q_c
							* dt
							* W.calc(X[step - 1][len - 1], T[step - 1][len - 1]) + T[step - 1][len - 1])
					+ kappa * dt * T[step - 1][len - 1];
			for (int i = 1; i < len - 2; ++i) {
				double tmpW = W.calc(X[step - 1][i + 1], T[step - 1][i + 1]);
				AX[i][i - 1] = AX[i][i + 1] = -D * dt;
				AX[i][i] = dz_2 + 2 * D * dt;
				bX[i] = (tmpW * dt + X[step - 1][i + 1]) * dz_2;

				AT[i][i - 1] = AT[i][i + 1] = -kappa * dt;
				AT[i][i] = dz_2 + 2 * kappa * dt;
				bT[i] = dz_2 * (-q_c * dt * tmpW + T[step - 1][i + 1]);
			}

			double[] tmpX = Thomas.solve(AX, bX);
			double[] tmpT = Thomas.solve(AT, bT);

			for (int iter = 0; iter < ITERATIONS; ++iter) {
				for (int i = 0; i < tmpX.length; ++i) {
					tmpX[i] = Math.max(0, tmpX[i]);
				}

				bX[0] = (W.calc(tmpX[0], tmpT[0]) * dt + tmpX[0]) * dz_2;
				bX[len - 2] = (W.calc(tmpX[tmpX.length - 1],
						tmpT[tmpT.length - 1]) * dt + tmpX[tmpX.length - 1])
						* dz_2 + D * dt * tmpX[tmpX.length - 1];
				bT[0] = dz_2 * (-q_c * dt * W.calc(tmpX[0], tmpT[0]) + tmpT[0])
						+ kappa * dt * T0;
				bT[len - 2] = dz_2
						* (-q_c
								* dt
								* W.calc(tmpX[tmpX.length - 1],
										tmpT[tmpT.length - 1]) + tmpT[tmpT.length - 1])
						+ kappa * dt * tmpT[tmpT.length - 1];

				for (int i = 1; i < len - 2; ++i) {
					double tmpW = W.calc(tmpX[i], tmpT[i]);
					bX[i] = (tmpW * dt + tmpX[i]) * dz_2;
					bT[i] = dz_2 * (-q_c * dt * tmpW + tmpT[i]);
				}

				tmpX = Thomas.solve(AX, bX);
				tmpT = Thomas.solve(AT, bT);
			}

			for (int i = 0; i < tmpX.length; ++i) {
				tmpX[i] = Math.max(0, tmpX[i]);
			}

			for (int i = 1; i < len; ++i) {
				X[step][i] = tmpX[i - 1];
				T[step][i] = tmpT[i - 1];
			}
            fireChangeEvent();
		}

		double[][] V = new double[steps][len];
		for (int i = 0; i < steps; ++i) {
			for (int j = 0; j < len; ++j) {
				V[i][j] = -W.calc(X[i][j], T[i][j]);
			}
		}

		return new MatrixTriple(X, T, V);
	}

    @Override
    public void addChangeListener(ChangeListener listener) {
        listeners.add(listener);
    }

    public int getStep() {
        return step;
    }

    private void fireChangeEvent() {
        ChangeEvent e = new ChangeEvent(this);
        for (ChangeListener listener : listeners) {
            listener.stateChanged(e);
        }
    }
}
