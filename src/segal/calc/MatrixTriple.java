package segal.calc;

public class MatrixTriple {

	private double[][] X, T, V;

	public MatrixTriple(double[][] X, double[][] T, double[][] V) {
		this.X = X;
		this.T = T;
		this.V = V;
	}

	public double[][] getX() {
		return X;
	}

	public double[][] getT() {
		return T;
	}

	public double[][] getV() {
		return V;
	}
}
