package segal.calc;

public class EnergyFunction {

	private final double K, E, R;
	
	private double alpha;

	public EnergyFunction() {
		this.K = 1.6E6;
		this.E = 8E4;
		this.R = 8.314;

		this.alpha = 1;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	public double calc(double X, double T) {
		return -K * Math.pow(X, alpha) * Math.exp(-E / (R * T));
	}
}
